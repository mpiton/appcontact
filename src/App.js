import React, { Component} from 'react';
import Header from './components/interface/Header';
import Liste from './components/contact/Liste';
import {Provider} from './context';
import AddContact from './components/contact/AddContact';
import APropos from './components/pages/APropos';
import Error from './components/pages/Error'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import "bootstrap/dist/css/bootstrap.min.css";

class App extends Component{
   render() {
      return (
    <Provider>
       <Router>
            <div className="App">
            <Header/>
            <div className="container">
               <Switch>
            <Route exact path="/ajoute" component={AddContact}/>
            <Route exact path="/apropos" component={APropos}/>
               <Route exact path="/liste" component={Liste}/>
               <Route component={Error}/>
               </Switch>
            </div>
            </div>
      </Router>
    </Provider>
  );
   }
}

export default App;
