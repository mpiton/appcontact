import React from 'react'

export default function Error() {
   return (
      <div>
         <h1 className="display-3">Erreur 404 : Page non trouvée.</h1>
      </div>
   )
}
